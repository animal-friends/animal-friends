package com.af.control;

import com.af.model.Animal;
import com.af.model.AnimalSearch;
import com.af.model.User;
import com.af.repository.AnimalRepository;
import com.af.repository.UserRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;


@Controller
public class AnimalController {

	private static final Logger log = LogManager.getLogger(AnimalController.class);

	private static String UPLOADED_FOLDER = "images/";

	@Autowired
	private AnimalRepository animalRepository;
	@Autowired
	private UserRepository userRepository;

	/**
	 * Página principal, muestra todos los animales de la base de datos
	 * @param model
	 * @return
	 */
	@GetMapping("/")
	public String index(Model model) {
		Iterable<Animal> animales = animalRepository.findAll();
		model.addAttribute("animales", animales);
		model.addAttribute("animalBusq", new AnimalSearch());
		return "index";
	}

	@GetMapping("/info/{idAnimal}")
	public String anuncio(@PathVariable Long idAnimal, Model model) {
		Optional<Animal> animal = animalRepository.findById(idAnimal);
		if (animal.isPresent()) {
			System.out.println(animal.toString());
			animal.get().setVisitas(animal.get().getVisitas() + 1);
			animalRepository.save(animal.get());
			model.addAttribute("animal", animal.get());

			String username = SecurityContextHolder.getContext().getAuthentication().getName();
			model.addAttribute("canDelete", username.equals(animal.get().getDonor().getEmail()));
			System.out.println(username.equals(animal.get().getDonor().getEmail()));
			return "info";
		}
		log.info("anuncio - OUT");
		return "redirect:/";
	}

	@GetMapping("/info/images/{idAnimal}")
	public StreamingResponseBody getImagen(@PathVariable Long idAnimal, Model model) throws IOException {
		File file = new File(UPLOADED_FOLDER + idAnimal);
		InputStream in = null;
		try {
			in = new BufferedInputStream(new FileInputStream(file));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		InputStream finalIn = in;
		return new StreamingResponseBody() {
			@Override
			public void writeTo(OutputStream out) throws IOException {
				FileCopyUtils.copy(finalIn, out);
			}
		};
	}

	@PostMapping("/busqueda")
	public String publicarAnuncio(@ModelAttribute AnimalSearch animal, Model model) {

		if (animal.getTamMin() == null) {
			animal.setTamMin(Integer.MIN_VALUE);
		}
		if (animal.getTamMax() == null) {
			animal.setTamMax(Integer.MAX_VALUE);
		}
		if (animal.getEdadMin() == null) {
			animal.setEdadMin(Integer.MIN_VALUE);
		}
		if (animal.getEdadMax() == null) {
			animal.setEdadMax(Integer.MAX_VALUE);
		}

		List<Animal> animales = animalRepository.findAllByTipoContainingAndUbicacionContainingAndTamBetweenAndEdadBetweenAndSexoContaining(
				animal.getTipo(), animal.getUbicacion(), animal.getTamMin(), animal.getTamMax(), animal.getEdadMin(), animal.getEdadMax(), animal.getSexo()
		);

		if (!animales.isEmpty()) {
			System.out.println(animales.toString());
			model.addAttribute("animales", animales);
			model.addAttribute("animalBusq", new AnimalSearch());
			return "index";
		}

		log.info("busqueda - OUT");
		return "redirect:/";
	}

	@PostMapping("/borrar/{id}")
	public String borrarAnuncio(@PathVariable Long id) {

		Optional<Animal> animal = animalRepository.findById(id);
		String username = SecurityContextHolder.getContext().getAuthentication().getName();
		if (animal.isPresent() && username.equals(animal.get().getDonor().getEmail())) {
			animalRepository.deleteById(id);
			return "redirect:/";
		}
		throw new ResponseStatusException(HttpStatus.I_AM_A_TEAPOT, "Not Found"); // ;)
	}

	@PostMapping("/publicar")
	public String publicarAnuncio(@ModelAttribute Animal animal, @RequestParam MultipartFile file) {
		String name = SecurityContextHolder.getContext().getAuthentication().getName();
		User user = userRepository.findByEmail(name);
		animal.setDonor(user);
		animalRepository.save(animal);

		Path dir = Paths.get(UPLOADED_FOLDER);

		try {
			byte[] bytes = file.getBytes();
			Path path = Paths.get(UPLOADED_FOLDER + animal.getId());
			Files.write(path, bytes);
			animal.setImage(path.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}

		animalRepository.save(animal);

		return "redirect:/info/" + animal.getId();
	}

	/*
		Devuelve la página de creación del anuncio. Crea un nuevo animal para que se rellene con el formulario.
	 */
	@GetMapping("/publicar")
	public String publicar(Model model) {
		model.addAttribute("animal", new Animal());
		model.addAttribute("animalBusq", new AnimalSearch());
		return "create";
	}
}
$(function () {
	switch (getFirstDirectoryPathname()) {
		case "":
			$("#index").addClass("active");
			break;
        case "publicar":
			$("#pub").addClass("active");
            break;

	}
	$('#tamanoAnimal').val("");
	$('#ageAnimal').val("");
    if (document.location.href.match(/[^\/]+$/)[0] === "publicar") {
        bootstrapValidate('#nameAnimal', 'required: Debes introducir un nombre para el animal')
        bootstrapValidate('#tipoAnimal', 'required: Debes introducir un tipo para el animal')
        bootstrapValidate('#razaAnimal', 'required: Debes introducir una raza para el animal')
        bootstrapValidate('#ageAnimal', 'integer: Introduce una edad valida')
        bootstrapValidate('#tamanoAnimal', 'integer: Introduce un tamaño en centímetros valida')
        bootstrapValidate('#descAnimal', 'min:60: Debes introducir al menos 60 caracteres')
        bootstrapValidate('#emailAn', 'email: Debes introducir un email válido')


	}

});


var getFirstDirectoryPathname = function() {
	var first = $(location).attr('pathname');
	first.indexOf(1);
	first.toLowerCase();
	return first.split("/")[1];
}

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};
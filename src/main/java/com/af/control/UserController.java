package com.af.control;

import com.af.model.User;
import com.af.repository.UserRepository;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;
import java.util.regex.Pattern;

@Controller
public class UserController {
	private UserRepository userRepo;
	private PasswordEncoder passwordEncoder;

	public UserController(UserRepository userRepo, PasswordEncoder passwordEncoder) {
		this.userRepo = userRepo;
		this.passwordEncoder = passwordEncoder;
	}

	@GetMapping("/register")
	public String registerForm() {
		return "registration";
	}

	@PostMapping("/register")
	public String processRegistration(User user) {
		Pattern pttText = Pattern.compile("^[a-zA-Zñ\\s]+$");
		Pattern pttDate = Pattern.compile("^([0-2][0-9]|(3)[0-1])(\\/)(((0)[0-9])|((1)[0-2]))(\\/)\\d{4}$");
		Pattern pttTel = Pattern.compile("^(\\\\+34|0034|34)?[6|7|8|9][0-9]{8}$");
		Pattern pttCP = Pattern.compile("^(?:0[1-9]|[1-4]\\d|5[0-2])\\d{3}$");
		Pattern pttEmail = Pattern.compile("^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$");
		Pattern pttPass = Pattern.compile("^(?=.*\\d)(?=.*[\\u0021-\\u002b\\u003c-\\u0040])(?=.*[A-Z])(?=.*[a-z])\\S{8,16}$");


		if (pttText.matcher(user.getName()).find() && pttText.matcher(user.getSurnames()).find() && pttText.matcher(user.getState()).find()
				&& pttText.matcher(user.getCity()).find() && pttDate.matcher(user.getBirthday()).find() && pttTel.matcher(user.getMobile()).find()
				&& pttCP.matcher(user.getPostcode()).find() && pttEmail.matcher(user.getEmail()).find() && pttPass.matcher(user.getPassword()).find()
				&& userRepo.findByEmail(user.getEmail()) == null) {
			user.setPassword(passwordEncoder.encode(user.getPassword()));
			userRepo.save(user);
			return "redirect:/login";
		} else {
			return "registration";
		}
	}

	@GetMapping("/exist/{username}")
	public @ResponseBody
	Boolean existUser(@PathVariable String username) {
		if (userRepo.findByEmail(username) != null) {
			return true;
		}
		return false;
	}

	@GetMapping("/login")
	public String login() {
		return "login";
	}

	@GetMapping("/login-error")
	public String loginError(Model model) {
		model.addAttribute("loginError", true);
		return "login";
	}

	@GetMapping("/profile")
	public String showProfile(Model model) {
		String name = SecurityContextHolder.getContext().getAuthentication().getName();
		model.addAttribute("user", userRepo.findByEmail(name));
		return "profile";
	}

	@PostMapping("/modify/{id}")
	public String modifyProfile(User newUser, @PathVariable Long id) {

		Pattern pttText = Pattern.compile("^[a-zA-Zñ\\s]+$");
		Pattern pttTel = Pattern.compile("^(\\\\+34|0034|34)?[6|7|8|9][0-9]{8}$");
		Pattern pttCP = Pattern.compile("^(?:0[1-9]|[1-4]\\d|5[0-2])\\d{3}$");
		Pattern pttPass = Pattern.compile("^(?=.*\\d)(?=.*[\\u0021-\\u002b\\u003c-\\u0040])(?=.*[A-Z])(?=.*[a-z])\\S{8,16}$");
		Optional<User> user = userRepo.findById(id);

		if (user.isPresent()) {
			if (newUser.getPassword() != null) {
				if (pttPass.matcher(newUser.getPassword()).find()) {
					user.get().setPassword(passwordEncoder.encode(newUser.getPassword()));
					userRepo.save(user.get());
					return "redirect:/profile";
				} else {
					return "redirect:/login";
				}
			} else {
				if (newUser.getMobile() != null && pttTel.matcher(newUser.getMobile()).find()) {
					user.get().setMobile(newUser.getMobile());
				}

				user.get().setAddress(newUser.getAddress());

				if (newUser.getCity() != null && pttText.matcher(newUser.getCity()).find()) {
					user.get().setCity(newUser.getCity());
				}

				if (newUser.getState() != null && pttText.matcher(newUser.getState()).find()) {
					user.get().setState(newUser.getState());
				}

				if (newUser.getPostcode() != null && pttCP.matcher(newUser.getPostcode()).find()) {
					user.get().setPostcode(newUser.getPostcode());
				}

				userRepo.save(user.get());
				return "redirect:/profile";
			}
		}
		return "redirect:/login";

	}
}


package com.af.repository;

import com.af.model.Animal;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface AnimalRepository extends CrudRepository<Animal, Long> {
	List<Animal> findAllByTipoContainingAndUbicacionContainingAndTamBetweenAndEdadBetweenAndSexoContaining(String tipo, String ubicacion, int tamMin, int tamMax, int edadMin, int edadMax, String sexo);
}

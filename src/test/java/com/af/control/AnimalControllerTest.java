package com.af.control;

import com.af.model.Animal;
import com.af.model.User;
import com.af.repository.AnimalRepository;
import com.af.repository.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class AnimalControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private AnimalRepository animalRepository;

    @Autowired
    private UserRepository userRepository;

    // Valores de inicialización que tendrá la base de datos

    private static final String desc = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime cumque voluptate consequuntur dicta dolores porro natus atque voluptatem minus eius soluta rem";

    private static Animal a1 = new Animal(1L,desc,5,"images/1","Guantes","gato","macho",60,"egipcio","Barcelona",0);
    private static Animal a2 = new Animal(2L,desc,9,"images/2","Excalibur","perro","hembra",125,"american stafford","Madrid",0);
    private static Animal a3 = new Animal(3L,desc,1,"images/3","Col","caracoles","hermafroditas",5,"normal","Almeria",0);

    /*
        Este test debería hacer la búsqueda sin filtros. Dado que el servidor se crea con tres animales de prueba,
        se hace el test con estos animales. Crear nuevo animales o eliminar los existentes cambiarían el estado de la
        base de datos con su ejecución, que es contrario a lo que debería hacer un test
    */
    @Test
    public void testIndex() throws Exception {
        mockMvc.perform(get("/"))
                .andExpect(status().isOk())
                .andExpect(view().name("index"))
                .andExpect(model().attribute("animales",
                        hasItem(a1)))
                .andExpect(model().attribute("animales",
                        hasItem(a2)))
                .andExpect(model().attribute("animales",
                        hasItem(a3)));
    }

    /*
        Este tes debería comprobar una muestra de datos concretos, por lo cual, el modelo contendrá el animal cuyo id
        se ha especificado. Haremos el test con los tres animales de manera individual dentro del test
     */
    @Test
    public void testInfo() throws Exception {
        mockMvc.perform(get("/info/1"))
                .andExpect(status().isOk())
                .andExpect(view().name(("info")))
                .andExpect(model().attribute("animal",
                        equalTo(a1)));
        mockMvc.perform(get("/info/2"))
                .andExpect(status().isOk())
                .andExpect(view().name(("info")))
                .andExpect(model().attribute("animal",
                        equalTo(a2)));
        
       // mockMvc.perform(get("/info/" + a3.getId()))
        mockMvc.perform(get("/info/3"))
                .andExpect(status().isOk())
                .andExpect(view().name(("info")))
                .andExpect(model().attribute("animal",
                        equalTo(a3)));
    }

    /*
        Con el fin de hacer que la base de datos quede igual tras la ejecución del test, primero se creará un anuncio
        temporal para posteriormente borrarlo. Se hará la comprobación de que existe como precondición  y de que se ha
        borrado como postcondición sobre ese anuncio.
     */
    @Test
    @WithMockUser(username = "test@af.com", roles="USER")
    public void testBorrarAnuncio() throws Exception {
        Animal a4 = new Animal("Animal 4", "tipo", "raza", 0, 0, "sexo", "ubicacion", "descripcion", 0);
        Iterable<User> users = userRepository.findAll();
        User donor = userRepository.findByEmail("test@af.com");
        a4.setDonor(donor);
        a4 = animalRepository.save(a4);
        Animal temp = animalRepository.findById(a4.getId()).get();
        assertTrue(animalRepository.existsById(a4.getId()));
        mockMvc.perform(post("/borrar/" + a4.getId()))
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/"));
        assertFalse(animalRepository.existsById(a4.getId()));
    }


    @Test
    @WithMockUser(username = "test@af.com", roles="USER")
    public void testPublicarAnuncio() throws Exception{
        MockMultipartFile file = new MockMultipartFile("file", "file.txt", "text/plain", "hi".getBytes());
        Animal a5 = new Animal("Animal 5", "tipo", "raza", 0, 0, "sexo", "ubicacion", "desc", 0);
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.multipart("/publicar")
                .file(file)
                .param("nombre", a5.getNombre())
                .param("tipo", a5.getTipo())
                .param("raza", a5.getRaza())
                .param("edad", Integer.toString(a5.getEdad()))
                .param("tam",Integer.toString(a5.getTam()))
                .param("sexo", a5.getSexo())
                .param("ubicacion", a5.getUbicacion())
                .param("descripcion", a5.getDescripcion())
                .flashAttr("animal", new Animal()))
                .andExpect(status().isFound())
                .andReturn();

        String[] data = result.getResponse().getRedirectedUrl().split("/");
        long id = Long.parseLong(data[2]);

        assertTrue(animalRepository.existsById(id));
        animalRepository.deleteById(id);
        assertFalse(animalRepository.existsById(id));
    }


    @Test
    @WithMockUser(username = "test@af.com", roles="USER")
    public void testPublicar() throws Exception{
        mockMvc.perform(get("/publicar"))
                .andExpect(status().isOk())
                .andExpect(view().name("create"))
                .andExpect(model().attribute("animal", notNullValue()));
    }


    @Test
    public void testBuscar() throws Exception{
    /*	mockMvc.perform(post("/busqueda"))
    	.andExpect(status().isOk())
    	.andExpect(view().name("index"))
    	.andExpect(model().attribute(name, matcher))*/
        mockMvc.perform(post("/busqueda")
                .param("tipo", a1.getTipo())
                .param("ubicacion", a1.getUbicacion())
                .param("tamMin", Integer.toString(a1.getTam() - 1))
                .param("tamMax", Integer.toString(a1.getTam() + 1))
                .param("edadMax", Integer.toString(5))
                .param("edadMin", Integer.toString(0))
                .param("sexo", a1.getSexo()))
                .andExpect(view().name("index"))
                .andExpect(status().isOk());

        mockMvc.perform(post("/busqueda")
                .param("tipo", "desconocido")
                .param("ubicacion", "desconocido")
                .param("tamMin", Integer.toString(1000))
                .param("tamMax", Integer.toString(1000))
                .param("edadMax", Integer.toString(8))
                .param("edadMin", Integer.toString(4))
                .param("sexo", "otro"))
                .andExpect(view().name("redirect:/"))
                .andExpect(status().isFound());
    }
}
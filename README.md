# Introducción
Este documento incluye instrucciones para realizar la **instalación del IDE**, la **importación del proyecto**, **la instalación de la base de datos** y  el **despliegue de la aplicación web**. Asimismo, incluirá una **lista de errores típicos** y su posible solución. Esta guía se centrará únicamente en la instalación y configuración en Windows. 
 
# Instalación del IDE
Un **IDE (Integrated Development Environment)** no es más que un software que proporciona asistencia al programador en el momento de la codificación de un proyecto. El ejemplo más representativo es el de Eclipse, que dispone de plug-in para la mayoría de tecnologías de desarrollo software.

El entorno que se ha elegido para este proyecto ha sido IntelliJ de JetBrain visto que proporciona una excelente compatibilidad y experiencia en el desarrollo de aplicaciones basadas en Spring Boot y Maven.

## Descarga del entorno
Para realizar la descarga habrá que registrarse previamente en la web de Jetbrains para estudiantes: https://www.jetbrains.com/student/. De esta forma tendremos una licencia completa gratuita sobre todos los productos de la compañía, en concreto el de IntelliJ Ultimate (para desarrolladores Java).

Una vez tenemos cuenta de usuario en la web, podremos continuar hacia la página de descargas de este módulo. Automáticamente detectará el Sistema Operativo y la arquitectura de vuestra máquina, se elegirá la versión Ultimate (versión completa y sin restricciones del programa) y procederá a la bajada.

## Requisitos previos
Antes de realizar la instalación, se necesitará una dependencia para correr el entorno y nuestra aplicación web: **Java Development Kit 8 (JDK 8)**. Está disponible en el sitio de Oracle a través del siguiente enlace: https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html. Habrá que realizar la instalación siguiendo las instrucciones del asistente de instalación.

##  Instalación

El proceso comenzará a partir de ejecutar el asistente de instalación de IntelliJ que previamente se había descargado. Este permitirá importar una configuración previa situada en un espacio de trabajo de una versión anterior del software, esto se ignorará y se procederá a elegir la interfaz gráfica más cómoda y a seleccionar los **Plug-ins requeridos** para el funcionamiento del proyecto, la pantalla que parecida a la siguiente:   

![Ventana de elección de plug-in](https://www.jetbrains.com/help/img/idea/2018.3/ij_set_plugins.png)

Tan solo se necesitaran Java Frameworks, Web Development, Version Controls y Aplication Servers para este proyecto, el resto se deshabilitará pulsando el botón disponible para ello.

#  Importación del proyecto

Antes de comenzar con la importación del proyecto necesitaremos **tener localizada la URL del repositorio** de este proyecto en GitLab. Seguidamente, abriremos IntelliJ y nos recibirá una pantalla de bienvenida como esta: 

![Pantalla de bienvenida de IntelliJ](https://i.ibb.co/5M0qNYF/welcomescreen.pnghttps://ibb.co/dWTndbg)

Pulsaremos sobre **«Check out from Versión Control > Git»**, se abrirá una ventana donde pulsaremos sobre el botón de radio «Import project from external model» y seleccionaremos Maven. Esto es debido a que las dependencias necesarias para construir nuestro proyecto están gestionadas por una herramienta externa llamada Maven que hará las importaciones de estas dependencias de forma automática y transparente al programador. 

![Pantalla de configuración de Maven](https://i.ibb.co/8Dr8v3p/Captura-de-pantalla-2019-03-24-a-las-12-44-05.png)

El siguiente paso nos permitirá seleccionar los parámetros para configurar Maven al gusto: no realizaremos ningún cambio salvo activar el botón de **«Import Maven projects automatically»**. Seguiremos pulsando el botón de siguiente hasta la pantalla de selección del SDK.

![Pantalla de selección del JDK 8](https://i.ibb.co/sv99NPZ/Captura-de-pantalla-2019-03-24-a-las-13-28-40.png)

> IntelliJ detectará en la mayoría de los casos la ruta donde se encuentra el Kit de Desarrollo de Java. De no ser así habrá que indicarlo manualmente de la siguiente manera:
> - Pulsaremos sobre el botón «+» de la esquina superior izquierda y elegiremos JDK desde el desplegable.
> - En la ventana de explorador de ficheros de Windows que aparecerá seleccionamos la ruta donde se encuentra instalado el JDK 8 (típicamente su ruta es: C:\Program Files\Java\jdk1.x.yy) y escogemos el directorio entero.

Tras realizar esto, ya tendríamos asignado un JDK a nuestro proyecto. Seguimos avanzando y pulsamos sobre finalizar. Ya tendremos nuestro proyecto importado, pero aun no estará preparado para su ejecución y despliegue.

# Instalación de la BD MariaDB

La aplicación necesitará tener una instancia de una Base de Datos ejecutándose en el ordenador para almacenar datos de manera persistente, por esto necesitaremos un Sistema Gestor de Bases de Datos que, en nuestro caso, será MariaDB. En este apartado explicaremos cómo realizar la instalación del SGBD y la configuración de una base de datos y sus usuarios/roles.

## Descarga

Para comenzar nos descargaremos, desde la URL oficial, el paquete de instalación: https://downloads.mariadb.org/mariadb/, seleccionando la versión estable y para Windows de 64 bits. 
## Instalación

Una vez descargado el paquete de instalación, procedemos a su instalación. 

En primer lugar, en la pantalla de «Default instance properties» pulsamos sobre «Modify password for database user 'root'». En la siguiente pantalla no realizamos cambios, pero comprobaremos que el puerto indicado en el apartado «Enable networking» sea el 3306 (puerto TCP por defecto usado para comunicaciones con SGBD de MySQL). Finalmente, pulsamos siguiente y terminamos.

MariaDB ya estaría instalado en la máquina y preparado para ser ejecutado.

## Configuración
Centrándonos en nuestro proyecto necesitaremos configurar una nueva base de datos, sus usuarios y los roles de los usuarios.

En primer lugar, **iniciaremos la instancia de la base de datos** como usuario root que permanecerá en localhost en el puerto de escucha 3306:

	$ mysql -u root -h localhost -p

En segundo lugar, **crearemos la base de datos** que se utilizará en el proyecto para almacenar la información generada por la aplicación con el siguiente comando:

	$ create database AF;

En último lugar, procederemos a **gestionar el usuario de la base de datos** que será el autor de las peticiones a esta:

	$ GRANT ALL PRIVILEGES ON AF.* to GPS@'localhost' IDENTIFIED BY 'spring';

*Nota: GPS es el nombre de usuario por defecto de nuestra aplicación; si este hubiese cambiado necesitaríamos actualizar la sentencia SQL anterior para adaptarla a la nueva configuración* 

# Despliegue en local de la aplicación

Finalmente, ya tenemos todo preparado para lanzar la aplicación en local, es decir, tu máquina actuará como servidor y cliente, y las peticiones *http (GET, POST, PUT, DELETE)* se realizarán dentro de esta.

Si todo lo anterior ha funcionado correctamente, tan solo tendríamos que dirigirnos a IntelliJ y  **pulsar el botón de la flecha verde** (play) situado en la **esquina superior derecha de la ventana** y, en la terminal, comenzarían a ejecutarse los comando. Tras unos segundos, la aplicación ya estaría disponible para ser usada en un navegador a través de la dirección http://localhost:8080. 

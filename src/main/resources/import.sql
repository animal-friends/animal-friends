INSERT INTO user
VALUES (1,'Calle Falsa 123', '01/01/1970','Springfield','test@af.com','618388798','Ejemplo','$2a$10$TfuTL7BzQfc11JC8T4gD9OQS5Cs2AFCup8ofsvPAIEkHOOVE7uh5S','28046','Misuri','Apellido de Ejemplo');

INSERT INTO animal
VALUES (1,
		'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime cumque voluptate consequuntur dicta dolores porro natus atque voluptatem minus eius soluta rem',
		5, 'images/1', 'Guantes', 'egipcio', 'macho', 60, 'gato', 'Barcelona', 0, 1),
	   (2,
		'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime cumque voluptate consequuntur dicta dolores porro natus atque voluptatem minus eius soluta rem',
		9, 'images/2', 'Excalibur', 'american stafford', 'hembra', 125, 'perro', 'Madrid', 0, 1),
	   (3,
		'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime cumque voluptate consequuntur dicta dolores porro natus atque voluptatem minus eius soluta rem',
		1, 'images/3', 'Col', 'normal', 'hermafroditas', 5, 'caracoles', 'Almeria', 0, 1);

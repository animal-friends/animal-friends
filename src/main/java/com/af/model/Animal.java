package com.af.model;

import javax.persistence.*;

@Entity
public class Animal {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private String nombre;
	private String tipo;
	private String raza;
	private int edad;
	private int tam; //en cm
	private String sexo;
	private String ubicacion;
	@Column(length = 500)
	private String descripcion;
	private int visitas;
	private String image;
	@ManyToOne
	private User donor;

	public Animal() {
	}

	public Animal(String nombre, String tipo, String raza, int edad, int tam, String sexo, String ubicacion, String descripcion, int visitas) {
		this.nombre = nombre;
		this.tipo = tipo;
		this.raza = raza;
		this.edad = edad;
		this.tam = tam;
		this.sexo = sexo;
		this.ubicacion = ubicacion;
		this.descripcion = descripcion;
		this.visitas = visitas;
	}

	public Animal(long id, String descripcion, int edad, String image, String nombre, String tipo, String sexo, int tam, String raza, String ubicacion, int visitas) {
		this.id = id;
		this.descripcion = descripcion;
		this.edad = edad;
		this.image = image;
		this.nombre = nombre;
		this.tipo = tipo;
		this.sexo = sexo;
		this.raza = raza;
		this.tam = tam;
		this.ubicacion = ubicacion;
		this.visitas = visitas;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getRaza() {
		return raza;
	}

	public void setRaza(String raza) {
		this.raza = raza;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public int getTam() {
		return tam;
	}

	public void setTam(int tam) {
		this.tam = tam;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getUbicacion() {
		return ubicacion;
	}

	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int getVisitas() {
		return visitas;
	}

	public Animal setVisitas(int visitas) {
		this.visitas = visitas;
		return this;
	}

	public String getImage() {
		return image;
	}

	public String setImage(String image) {
		this.image = image;
		return String.valueOf(this);
	}

	public User getDonor() {
		return donor;
	}

	public void setDonor(User user) {
		this.donor = user;
	}

	@Override
	public String toString() {
		return "Animal{" +
				"id=" + id +
				", nombre='" + nombre + '\'' +
				", tipo='" + tipo + '\'' +
				", raza='" + raza + '\'' +
				", edad=" + edad +
				", tam=" + tam +
				", sexo='" + sexo + '\'' +
				", ubicacion='" + ubicacion + '\'' +
				", descripcion='" + descripcion + '\'' +
				", visitas=" + visitas +
				", image='" + image + '\'' +
				", user=" + donor +
				'}';
	}

	// Compara solo aquellos datos fijos y atómicos
	@Override
	public boolean equals(Object animal) {
		boolean result;

		result = id == ((Animal) animal).getId();
		result = result && nombre.equals(((Animal) animal).getNombre());
		result = result && tipo.equals(((Animal) animal).getTipo());
		result = result && raza.equals(((Animal) animal).getRaza());
		result = result && edad == ((Animal) animal).getEdad();
		result = result && tam == ((Animal) animal).getTam();
		result = result && sexo.equals(((Animal) animal).getSexo());
		result = result && ubicacion.equals(((Animal) animal).getUbicacion());
		result = result && descripcion.equals(((Animal) animal).getDescripcion());

		return result;
	}
}

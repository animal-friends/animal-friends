'use strict';

function validTextInput() {
	var isValid = true;
	var rgxDate = RegExp("^([0-2][0-9]|(3)[0-1])(\\/)(((0)[0-9])|((1)[0-2]))(\\/)\\d{4}$");
	var rgxText = RegExp("^[a-zA-Zñ\\s]+$");
	var rgxTel = RegExp("^(\\+34|0034|34)?[6|7|8|9][0-9]{8}$");
	var rgxEmail = RegExp("^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$");
	var rgxPass = new RegExp("^(?=.*\\d)(?=.*[\u0021-\u002b\u003c-\u0040])(?=.*[A-Z])(?=.*[a-z])\\S{8,16}$");
	var rgxCP = new RegExp("^(?:0[1-9]|[1-4]\\d|5[0-2])\\d{3}$")
	var name = $("#name");
	var surnames = $("#surnames");
	var birthday = $("#birthday");
	var tel = $("#tel");
	var email = $("#email");
	var city = $("#city");
	var state = $("#state");
	var pass = $("#pass");
	var pass2 = $("#pass2");
	var cp = $("#postcode");
	var addr = $("#address");

	if (name.val() === "" || !rgxText.test(name.val())) {
		name.removeClass("is-valid");
		name.addClass("is-invalid");
		isValid = false;
	} else {
		name.removeClass("is-invalid");
		name.addClass("is-valid");
	}

	if (surnames.val() === "" || !rgxText.test(surnames.val())) {
		surnames.removeClass("is-valid");
		surnames.addClass("is-invalid");
		isValid = false;
	} else {
		surnames.removeClass("is-invalid");
		surnames.addClass("is-valid");
	}

	if (birthday.val() === "" || !rgxDate.test(birthday.val())) {
		birthday.removeClass("is-valid");
		birthday.addClass("is-invalid");
		isValid = false;
	} else {
		birthday.removeClass("is-invalid");
		birthday.addClass("is-valid");
	}

	if (tel.val() === "" || !rgxTel.test(tel.val())) {
		tel.removeClass("is-valid");
		tel.addClass("is-invalid");
		isValid = false;
	} else {
		tel.removeClass("is-invalid");
		tel.addClass("is-valid");
	}

	if (email.val() === "" || !rgxEmail.test(email.val())) {
		email.removeClass("is-valid");
		email.addClass("is-invalid");
		$("#email-feedback").text("Campo requerido. El correo electrónico no es válido");
		isValid = false;
	} else {
		$.ajax({
			url: "http://localhost:8080/exist/" + email.val(), async: false,success: function (result) {
				if (result) {
					$("#email-feedback").text("El correo electrónico ya existe");
					email.removeClass("is-valid");
					email.addClass("is-invalid");
					isValid = false;
				} else {
					email.removeClass("is-invalid");
					email.addClass("is-valid");
				}
			}, error: function (result) {
				alert("Error inesperado. La conexión con el servidor ha fallado.");
			}
		});
	}

	if (city.val() === "" || !rgxText.test(city.val())) {
		city.removeClass("is-valid");
		city.addClass("is-invalid");
		isValid = false;
	} else {
		city.removeClass("is-invalid");
		city.addClass("is-valid");
	}

	if (state.val() === "" || !rgxText.test(state.val())) {
		state.removeClass("is-valid");
		state.addClass("is-invalid");
		isValid = false;
	} else {
		state.removeClass("is-invalid");
		state.addClass("is-valid");
	}

	if (cp.val() === "" || !rgxCP.test(cp.val())) {
		cp.removeClass("is-valid");
		cp.addClass("is-invalid");
		isValid = false;
	} else {
		cp.removeClass("is-invalid");
		cp.addClass("is-valid");
	}

	if (!rgxPass.test(pass.val()) || pass.val() !== pass2.val()) {
		pass.removeClass("is-valid");
		pass.addClass("is-invalid");
		pass2.removeClass("is-valid");
		pass2.addClass("is-invalid");
		isValid = false;
	} else {
		pass.removeClass("is-invalid");
		pass.addClass("is-valid");
		pass2.removeClass("is-invalid");
		pass2.addClass("is-valid");
	}

	if (addr.val() === "") {
		addr.removeClass("is-valid");
		addr.addClass("is-invalid");
		isValid = false;
	} else {
		addr.removeClass("is-invalid");
		addr.addClass("is-valid");
	}
	return isValid;
}

function validTextEdit() {
	var isValid = true;
	var rgxTel = RegExp("^(\\+34|0034|34)?[6|7|8|9][0-9]{8}$");
	var rgxCP = new RegExp("^(?:0[1-9]|[1-4]\\d|5[0-2])\\d{3}$")
	var rgxText = RegExp("^[a-zA-Zñ\\s]+$");
	var tel = $("#tel");
	var city = $("#city");
	var state = $("#state");
	var cp = $("#postcode");
	var addr = $("#address");

	if (!rgxTel.test(tel.val())) {
		tel.removeClass("is-valid");
		tel.addClass("is-invalid");
		isValid = false;
	} else {
		tel.removeClass("is-invalid");
		tel.addClass("is-valid");
	}

	if (!rgxText.test(city.val())) {
		city.removeClass("is-valid");
		city.addClass("is-invalid");
		isValid = false;
	} else {
		city.removeClass("is-invalid");
		city.addClass("is-valid");
	}

	if (!rgxText.test(state.val())) {
		state.removeClass("is-valid");
		state.addClass("is-invalid");
		isValid = false;
	} else {
		state.removeClass("is-invalid");
		state.addClass("is-valid");
	}

	if (!rgxCP.test(cp.val())) {
		cp.removeClass("is-valid");
		cp.addClass("is-invalid");
		isValid = false;
	} else {
		cp.removeClass("is-invalid");
		cp.addClass("is-valid");
	}

	return isValid;
}

function validPass() {
	var rgxPass = new RegExp("^(?=.*\\d)(?=.*[\u0021-\u002b\u003c-\u0040])(?=.*[A-Z])(?=.*[a-z])\\S{8,16}$");
	var pass = $("#pass");
	var pass2 = $("#pass2");
	var isValid = true;

	if (!rgxPass.test(pass.val()) || pass.val() !== pass2.val()) {
		pass.removeClass("is-valid");
		pass.addClass("is-invalid");
		pass2.removeClass("is-valid");
		pass2.addClass("is-invalid");
		isValid = false;
	} else {
		pass.removeClass("is-invalid");
		pass.addClass("is-valid");
		pass2.removeClass("is-invalid");
		pass2.addClass("is-valid");
	}

	return isValid;
}
package com.af.security;



import com.af.model.User;

import com.af.repository.UserRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Optional;

import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class SecurityTest {

	@Autowired
	private MockMvc mockMvc;
	@Autowired
	private UserRepository userRepo;
	
	private static User u1 = new User(0, "test", "test@test.com", "$22Ahws8uuh5S", new ArrayList<>());

    private static boolean init = false;

    @Before
    public void setUp() {
        if(!init) {
        	//userRepo.deleteAll();
            init = true;
            userRepo.save(u1);
        }
    }
	
	@Test
	public void processRegistrationTest() throws Exception{
		String correo = "secondtest@af.com";
		User prueba = new User("name", "surname", "13/12/1996", "619619619", "aqui", "city", "state", "28030", correo, "$1245VE7uh5S", new ArrayList<>());
	
		//todos los datos correctamente
		mockMvc.perform(post("/register")
				.param("name", prueba.getName())
				.param("surnames", prueba.getSurnames())
				.param("birthday", prueba.getBirthday())
				.param("mobile", prueba.getMobile())
				.param("address", prueba.getAddress())
				.param("city", prueba.getCity())
				.param("state", prueba.getState())
				.param("postcode", prueba.getPostcode())
				.param("email", prueba.getEmail())
				.param("password", prueba.getPassword())
				.flashAttr("user", new User()))
		.andExpect(status().isFound())
		.andExpect(view().name("redirect:/login"));
		
		//introduciendo mal la fecha
		//prueba.setBirthday("13-02-2012");
		mockMvc.perform(post("/register")
				.param("name", prueba.getName())
				.param("surnames", prueba.getSurnames())
				.param("birthday", "13-02-2012")
				.param("mobile", prueba.getMobile())
				.param("address", prueba.getAddress())
				.param("city", prueba.getCity())
				.param("state", prueba.getState())
				.param("postcode", prueba.getPostcode())
				.param("email", prueba.getEmail())
				.param("password", prueba.getPassword())
				.flashAttr("user", new User()))
		.andExpect(status().isOk())
		.andExpect(view().name("registration"));
		
		//introduciendo contraseña inválida
		//prueba.setPassword("123456789h");
		mockMvc.perform(post("/register")
				.param("name", prueba.getName())
				.param("surnames", prueba.getSurnames())
				.param("birthday", prueba.getBirthday())
				.param("mobile", prueba.getMobile())
				.param("address", prueba.getAddress())
				.param("city", prueba.getCity())
				.param("state", prueba.getState())
				.param("postcode", prueba.getPostcode())
				.param("email", "correoprueba")
				.param("password", "123456789h")
				.flashAttr("user", new User()))
		.andExpect(status().isOk())
		.andExpect(view().name("registration"));
		
		mockMvc.perform(post("/register")
				.param("name", prueba.getName())
				.param("surnames", prueba.getSurnames())
				.param("birthday", prueba.getBirthday())
				.param("mobile", prueba.getMobile())
				.param("address", prueba.getAddress())
				.param("city", prueba.getCity())
				.param("state", prueba.getState())
				.param("postcode", prueba.getPostcode())
				.param("email","correoprue2ba")
				.param("password", "1234567A89h")
				.flashAttr("user", new User()))
		.andExpect(status().isOk())
		.andExpect(view().name("registration"));
		
		mockMvc.perform(post("/register")
				.param("name", prueba.getName())
				.param("surnames", prueba.getSurnames())
				.param("birthday", prueba.getBirthday())
				.param("mobile", prueba.getMobile())
				.param("address", prueba.getAddress())
				.param("city", prueba.getCity())
				.param("state", prueba.getState())
				.param("postcode", prueba.getPostcode())
				.param("email", "co2rreoprueba")
				.param("password", "1234567A89h")
				.flashAttr("user", new User()))
		.andExpect(status().isOk())
		.andExpect(view().name("registration"));
		
		mockMvc.perform(post("/register")
				.param("name", prueba.getName())
				.param("surnames", prueba.getSurnames())
				.param("birthday", prueba.getBirthday())
				.param("mobile", prueba.getMobile())
				.param("address", prueba.getAddress())
				.param("city", prueba.getCity())
				.param("state", prueba.getState())
				.param("postcode", prueba.getPostcode())
				.param("email", "co2rreopddrueba")
				.param("password", "12345678jA$$")
				.flashAttr("user", new User()))
		.andExpect(status().isOk())
		.andExpect(view().name("registration"));
		
		//codigo postal incorrecto
	//	prueba.setPostcode("25874857");
		mockMvc.perform(post("/register")
				.param("name", prueba.getName())
				.param("surnames", prueba.getSurnames())
				.param("birthday", prueba.getBirthday())
				.param("mobile", prueba.getMobile())
				.param("address", prueba.getAddress())
				.param("city", prueba.getCity())
				.param("state", prueba.getState())
				.param("postcode", "25874857")
				.param("email", "correo3prueba")
				.param("password", prueba.getPassword())
				.flashAttr("user", new User()))
		.andExpect(status().isOk())
		.andExpect(view().name("registration"));
		
		// telefono incorrecto
		//prueba.setMobile("547874258");
		mockMvc.perform(post("/register")
				.param("name", prueba.getName())
				.param("surnames", prueba.getSurnames())
				.param("birthday", prueba.getBirthday())
				.param("mobile", "547874258")
				.param("address", prueba.getAddress())
				.param("city", prueba.getCity())
				.param("state", prueba.getState())
				.param("postcode", prueba.getPostcode())
				.param("email", "correoprueba33")
				.param("password", prueba.getPassword())
				.flashAttr("user", new User()))
		.andExpect(status().isOk())
		.andExpect(view().name("registration"));
		
		assertTrue(userRepo.existsByEmail(correo));
		long id = userRepo.findByEmail(correo).getId();
		userRepo.deleteById(id);
		assertFalse(userRepo.existsByEmail(correo));
	}
	

	@Test
	public void existUserTest() throws Exception {
		
		//este usuario no existe en nuestra base de datos
		
		mockMvc.perform(get("/exist/CorreoNoExistente@ucm.com"))
				.andExpect(status().isOk())
				.andExpect(content().string("false"));
		
		//este usuario si existe en nuestra base de datos
		
		mockMvc.perform(get("/exist/test@test.com"))
		.andExpect(status().isOk())
		.andExpect(content().string("true"));

	}
	
	@Test
	public void RegisterTest() throws Exception {
		mockMvc.perform(get("/register"))
		.andExpect(status().isOk())
		.andExpect(view().name("registration"));
	}
	
	@Test
	public void LoginErrorTest() throws Exception{
		mockMvc.perform(get("/login-error"))
		.andExpect(status().isOk())
		.andExpect(view().name("login"));
	}
	
	@Test
    @WithMockUser(username = "test@test.com", roles = "USER")
    public void ShowProfileTest() throws Exception {
         mockMvc.perform(get("/profile"))
            .andExpect(status().isOk())
            .andExpect(view().name("profile"))
            .andExpect(model().attribute("user",
                    hasProperty("email", is("test@test.com"))));
    }
	
	@Test
	public void mofidyProfileTest() throws Exception{
		/*
		 * Modificamos los parámetros del usuario u1 de prueba y todo okey
		 */
		u1.setCity("Alcorcon");
		u1.setPostcode("28999");
		u1.setMobile("654789874");
		mockMvc.perform(post("/modify/"+ u1.getId())
				.param("address", u1.getAddress())
				.param("city", u1.getCity())
				.param("mobile",u1.getMobile())
				.param("state", u1.getState())
				.param("password", u1.getPassword())
				.param("postcode", u1.getPostcode()))
		.andExpect(status().isFound())
		.andExpect(view().name("redirect:/profile"));
		
		/*
		 * Modificamos los parámetros del usuario u1 de prueba y falle teléfono
		 */
		u1.setCity("Alcorcon");
		u1.setPostcode("28999");
		u1.setMobile("154789874");
		mockMvc.perform(post("/modify/"+ u1.getId())
				.param("address", u1.getAddress())
				.param("city", u1.getCity())
				.param("mobile",u1.getMobile())
				.param("state", u1.getState())
				.param("password", u1.getPassword())
				.param("postcode", u1.getPostcode()))
		.andExpect(status().isFound())
		.andExpect(view().name("redirect:/profile"));
		
		/*
		 * Modificamos los parámetros del usuario u1 de prueba y falle teléfono
		 */
		u1.setCity("Alcorcon");
		u1.setPostcode("28934499");
		u1.setMobile("754789874");
		mockMvc.perform(post("/modify/"+ u1.getId())
				.param("address", u1.getAddress())
				.param("city", u1.getCity())
				.param("mobile",u1.getMobile())
				.param("state", u1.getState())
				.param("password", u1.getPassword())
				.param("postcode", u1.getPostcode()))
		.andExpect(status().isFound())
		.andExpect(view().name("redirect:/profile"));
		
		
		
		
		
		
		/*
		 * Prueba con un usuario que no existe
		 */
		User prueba = new User("name", "surname", "13/12/1996", "619619619", "aqui", "city", "state", "28030", "prueba@joder.com", "up8ofsvPAI$de", new ArrayList<>());
		mockMvc.perform(post("/modify/"+ prueba.getId())
				.param("address", prueba.getAddress())
				.param("city", prueba.getCity())
				.param("mobile",prueba.getMobile())
				.param("state", prueba.getState())
				.param("postcode", prueba.getPostcode()))
		.andExpect(status().isFound())
		.andExpect(view().name("redirect:/login"));
	}

	
}

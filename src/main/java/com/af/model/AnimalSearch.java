package com.af.model;

public class AnimalSearch {
	private String ubicacion;
	private String sexo;
	private String tipo;
	private Integer edadMax;
	private Integer edadMin;
	private Integer tamMax;
	private Integer tamMin;

	public AnimalSearch() {}

	public AnimalSearch(String ubicacion, String sexo, String tipo, int edadMax, int edadMin, int tamMax, int tamMin) {
		this.ubicacion = ubicacion;
		this.sexo = sexo;
		this.tipo = tipo;
		this.edadMax = edadMax;
		this.edadMin = edadMin;
		this.tamMax = tamMax;
		this.tamMin = tamMin;
	}

	public String getUbicacion() {
		return ubicacion;
	}

	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Integer getEdadMax() {
		return edadMax;
	}

	public void setEdadMax(Integer edadMax) {
		this.edadMax = edadMax;
	}

	public Integer getEdadMin() {
		return edadMin;
	}

	public void setEdadMin(Integer edadMin) {
		this.edadMin = edadMin;
	}

	public Integer getTamMax() {
		return tamMax;
	}

	public void setTamMax(Integer tamMax) {
		this.tamMax = tamMax;
	}

	public Integer getTamMin() {
		return tamMin;
	}

	public void setTamMin(Integer tamMin) {
		this.tamMin = tamMin;
	}
}
